﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardPrinterIssuing
{
    public class Constants
    {
        public class Settings
        {
            public const string FolderImage = @"C:\CardIssuePrinter";
            public const string FileConfig = @"printer.config";
            public const string LBPrinterName = "Printer Name:";
        }

        public class MessageProtocolType
        {
            public const string PrinterStatus = "PRINTER_STATUS";
            public const string PrinterCard = "PRINTER_CARD";
            public const string PrinterReadData = "PRINTER_READ_DATA";
            public const string PrinterWriteData = "PRINTER_WRITE_DATA";
        }

        public class MessageStatus
        {
            public const int Success = 1;
            public const int Error = 0;
        }

        public static class ConfigSocket
        {
            public static int DataLength = 1024 * 2;
            public static int Port = 4949;
            public static int ClientLength = 100;
        }

        public enum PrinterType
        {
            EntrustSD360 = 0,
            CXD80 = 1,
        }
    }
}
