﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardPrinterIssuing.Models
{
    public class IssueData
    {
        public string CardId { get; set; }
        public int UserId { get; set; }
        public int IssueCount { get; set; }
        public DateTime PersoDate { get; set; }

        public DateTime StartValid { get; set; }
        public DateTime EndValid { get; set; }
        public bool UseCardId { get; set; }
    }
}
