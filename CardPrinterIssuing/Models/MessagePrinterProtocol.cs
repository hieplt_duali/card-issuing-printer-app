﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardPrinterIssuing.Models
{
    public class MessagePrinterProtocol
    {
        public string MsgId { get; set; }
        public string MessageType { get; set; }
        public string PrinterName { get; set; }
        public int Total { get; set; }
        public int Index { get; set; }
        public string FileName { get; set; }
        public string CardId { get; set; }
        public string DataImage { get; set; }
        public List<DataIssuePrinter> DataIssuePrinters { get; set; }
        public byte[] KeyPrinter { get; set; }
        public IssueData IssueData { get; set; }
        public ConfigKey ConfigKey { get; set; }
    }

    public class MessagePrinterResponseProtocol
    {
        public string MsgId { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }


    public class DataIssuePrinter
    {
        public byte Sector { get; set; }
        public byte Block { get; set; }
        public byte[] Data { get; set; }
    }
}
