﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardPrinterIssuing.Models
{
    public class ConfigKey
    {
        public byte[] AuthenticationKey { get; set; }
        public byte[] AccessBits { get; set; }
        public string AesIv { get; set; }
        public string AesKey { get; set; }
    }
}
