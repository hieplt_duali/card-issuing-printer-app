﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardPrinterIssuing.Models
{
    public class CardPrintModel
    {
        public string MsgId { get; set; }
        public string Time { get; set; }
        public string PrinterName { get; set; }
        public string CardId { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
    }
}
