﻿using CardPrinterIssuing.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;

namespace CardPrinterIssuing
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public MainWindow(short printerType)
        {
            // set variable local
            _printerType = printerType;

            InitializeComponent();

            // setup binding data
            InitBindingData();

            // setup config
            SetupConfig();

            // start socket
            Connect();

            // display socket server
            PrintAddressAndPort();
        }

        #region Helper socket tcp/ip

        // key: messageId, value: list message
        Dictionary<string, List<MessagePrinterProtocol>> MessageQueues = new Dictionary<string, List<MessagePrinterProtocol>>();

        IPEndPoint IP;
        Socket server;
        List<Socket> clientList;

        void Connect()
        {
            clientList = new List<Socket>();
            IP = new IPEndPoint(IPAddress.Any, Constants.ConfigSocket.Port);
            server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);

            server.Bind(IP);

            Thread listen = new Thread(() =>
            {
                try
                {
                    while (true)
                    {
                        server.Listen(Constants.ConfigSocket.ClientLength);
                        Socket client = server.Accept();

                        Thread receive = new Thread(Receive);
                        receive.IsBackground = true;
                        receive.Start(client);

                        clientList.Add(client);
                    }
                }
                catch
                {
                    IP = new IPEndPoint(IPAddress.Any, Constants.ConfigSocket.Port);
                    server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                }
            });
            listen.IsBackground = true;
            listen.Start();
        }

        void Send(Socket client, MessagePrinterResponseProtocol message)
        {
            if (message != null && client != null)
                client.Send(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message)));
        }

        void Receive(object obj)
        {
            Socket client = obj as Socket;
            try
            {
                while (true)
                {
                    byte[] data = new byte[Constants.ConfigSocket.DataLength];
                    client.Receive(data);

                    string messageText = Encoding.UTF8.GetString(data);

                    if (string.IsNullOrEmpty(messageText))
                    {
                        continue;
                    }

                    MessagePrinterProtocol message = JsonConvert.DeserializeObject<MessagePrinterProtocol>(messageText);
                    MessagePrinterResponseProtocol messageResponse = new MessagePrinterResponseProtocol();

                    if (message == null)
                    {
                        continue;
                    }

                    switch (message.MessageType)
                    {
                        case Constants.MessageProtocolType.PrinterStatus:
                            {
                                bool status = _printerType == (short)Constants.PrinterType.CXD80
                                    ? PrinterCXD80Helper.CheckConnection()
                                    : PrintHelper.BindDevice(message.PrinterName);
                                messageResponse = new MessagePrinterResponseProtocol()
                                {
                                    MsgId = message.MsgId,
                                    Status = status ? Constants.MessageStatus.Success : Constants.MessageStatus.Error,
                                };

                                break;
                            }
                        case Constants.MessageProtocolType.PrinterReadData:
                            {
                                string dataInCard = "";
                                bool status = false;
                                if(message.DataIssuePrinters != null && message.DataIssuePrinters.Any() && message.KeyPrinter != null)
                                {
                                    status = PrintHelper.ReadData(message.PrinterName, message.DataIssuePrinters[0].Block, message.DataIssuePrinters[0].Sector, message.KeyPrinter, out dataInCard);
                                }
                                else
                                {
                                    status = false;
                                    dataInCard = "Data failed.";
                                }

                                messageResponse = new MessagePrinterResponseProtocol()
                                {
                                    MsgId = message.MsgId,
                                    Status = status ? Constants.MessageStatus.Success : Constants.MessageStatus.Error,
                                    Message = dataInCard
                                };
                                break;
                            }
                        case Constants.MessageProtocolType.PrinterWriteData:
                            {
                                string msgError = "";
                                if (message.DataIssuePrinters != null && message.DataIssuePrinters.Any() && message.KeyPrinter != null)
                                {
                                    msgError = PrintHelper.WriteData(message.PrinterName, message.DataIssuePrinters[0].Block, message.DataIssuePrinters[0].Sector, message.KeyPrinter, message.DataIssuePrinters[0].Data);
                                }
                                else
                                {
                                    msgError = "Data failed.";
                                }

                                messageResponse = new MessagePrinterResponseProtocol()
                                {
                                    MsgId = message.MsgId,
                                    Status = string.IsNullOrEmpty(msgError) ? Constants.MessageStatus.Success : Constants.MessageStatus.Error,
                                    Message = msgError
                                };
                                break;
                            }
                        case Constants.MessageProtocolType.PrinterCard:
                            {
                                List<MessagePrinterProtocol> listMessage;
                                if (!MessageQueues.TryGetValue(message.MsgId, out listMessage))
                                {
                                    listMessage = new List<MessagePrinterProtocol>();
                                    MessageQueues.Add(message.MsgId, listMessage);
                                }
                                listMessage.Add(message);

                                if (listMessage.Count < message.Total)
                                {
                                    MessageQueues[message.MsgId] = listMessage;
                                    messageResponse = new MessagePrinterResponseProtocol()
                                    {
                                        MsgId = message.MsgId,
                                        Status = Constants.MessageStatus.Success
                                    };
                                }
                                else
                                {
                                    listMessage = listMessage.OrderBy(m => m.Index).ToList();

                                    // save file
                                    if (!Directory.Exists($"{Constants.Settings.FolderImage}/{message.MsgId}"))
                                    {
                                        Directory.CreateDirectory($"{Constants.Settings.FolderImage}/{message.MsgId}");
                                    }

                                    string dataImageText = "";
                                    foreach (var item in listMessage)
                                    {
                                        dataImageText += item.DataImage;
                                    }
                                    byte[] dataImage = Convert.FromBase64String(dataImageText);
                                    using (MemoryStream ms = new MemoryStream(dataImage))
                                    {
                                        Image image = Image.FromStream(ms);
                                        image.Save($"{Constants.Settings.FolderImage}/{message.MsgId}/{message.FileName}");
                                        ms.Dispose();
                                    }

                                    // check existed 2 file card_font and card_back
                                    if (File.Exists($"{Constants.Settings.FolderImage}/{message.MsgId}/card_font.jpeg") && File.Exists($"{Constants.Settings.FolderImage}/{message.MsgId}/card_back.jpeg"))
                                    {
                                        string msgError = "";
                                        string uid = "";
                                        if(_printerType == (short)Constants.PrinterType.EntrustSD360)
                                        {
                                            if (message.ConfigKey != null && message.IssueData != null)
                                            {
                                                // write card and print
                                                msgError = PrintHelper.WriteAndPrintCard(message.MsgId, message.PrinterName, message.IssueData, message.ConfigKey, out uid);
                                            }
                                            else
                                            {
                                                msgError = PrintHelper.PrintCard(message.MsgId, message.PrinterName);
                                            }
                                        }
                                        else
                                        {
                                            msgError = PrinterCXD80Helper.PrintCard($"{Constants.Settings.FolderImage}/{message.MsgId}/card_font.jpeg", $"{Constants.Settings.FolderImage}/{message.MsgId}/card_back.jpeg");
                                        }

                                        messageResponse = new MessagePrinterResponseProtocol()
                                        {
                                            MsgId = message.MsgId,
                                            Status = string.IsNullOrEmpty(msgError) ? Constants.MessageStatus.Success : Constants.MessageStatus.Error,
                                            Message = string.IsNullOrEmpty(msgError) ? uid : msgError
                                        };

                                        // Display history print
                                        AddItemForListPrintHistory(new CardPrintModel
                                        {
                                            MsgId = message.MsgId,
                                            Time = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"),
                                            PrinterName = message.PrinterName,
                                            CardId = (message.IssueData != null && message.IssueData.UseCardId) ? message.CardId : uid,
                                            Status = string.IsNullOrEmpty(msgError) ? "Success" : "Error",
                                            ErrorMessage = msgError
                                        });
                                    }
                                    else
                                    {
                                        messageResponse = new MessagePrinterResponseProtocol()
                                        {
                                            MsgId = message.MsgId,
                                            Status = Constants.MessageStatus.Success,
                                        };
                                    }
                                    try
                                    {
                                        // remove data in queue
                                        MessageQueues.Remove(message.MsgId);
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }

                                break;
                            }
                        default:
                            {
                                messageResponse = new MessagePrinterResponseProtocol()
                                {
                                    MsgId = message.MsgId,
                                    Status = Constants.MessageStatus.Error,
                                    Message = "Message wrong message type"
                                };
                                break;
                            }
                    }

                    // send mesage response
                    Send(client, messageResponse);

                    // intit label
                    PrinterName = Constants.Settings.LBPrinterName + " " + message?.PrinterName;
                }
            }
            catch (Exception ex)
            {

                //clientList.Remove(client);
                //client.Close();
            }
        }

        #endregion

        #region Init config

        public void SetupConfig()
        {
            if (!Directory.Exists(Constants.Settings.FolderImage))
            {
                Directory.CreateDirectory(Constants.Settings.FolderImage);
            }

            if (!File.Exists($"{Constants.Settings.FolderImage}/{Constants.Settings.FileConfig}"))
            {
                Dictionary<string, string> dataConfig = new Dictionary<string, string>();
                dataConfig.Add("DataLength", Constants.ConfigSocket.DataLength.ToString());
                dataConfig.Add("Port", Constants.ConfigSocket.Port.ToString());
                dataConfig.Add("ClientLength", Constants.ConfigSocket.ClientLength.ToString());
                File.WriteAllText($"{Constants.Settings.FolderImage}/{Constants.Settings.FileConfig}", JsonConvert.SerializeObject(dataConfig));
            }
            else
            {
                try
                {
                    string text = File.ReadAllText($"{Constants.Settings.FolderImage}/{Constants.Settings.FileConfig}");
                    Dictionary<string, string> dataConfig = JsonConvert.DeserializeObject<Dictionary<string, string>>(text);

                    Constants.ConfigSocket.DataLength = int.Parse(dataConfig["DataLength"]);
                    Constants.ConfigSocket.Port = int.Parse(dataConfig["Port"]);
                    Constants.ConfigSocket.ClientLength = int.Parse(dataConfig["ClientLength"]);
                }
                catch (System.Exception)
                {
                    File.Delete($"{Constants.Settings.FolderImage}/{Constants.Settings.FileConfig}");
                    Dictionary<string, string> dataConfig = new Dictionary<string, string>();
                    dataConfig.Add("DataLength", Constants.ConfigSocket.DataLength.ToString());
                    dataConfig.Add("Port", Constants.ConfigSocket.Port.ToString());
                    dataConfig.Add("ClientLength", Constants.ConfigSocket.ClientLength.ToString());
                    File.WriteAllText($"{Constants.Settings.FolderImage}/{Constants.Settings.FileConfig}", JsonConvert.SerializeObject(dataConfig));
                }
            }

            string[] dirNames = Directory.GetDirectories(Constants.Settings.FolderImage);
            if(dirNames.Length > 0)
            {
                foreach (var item in dirNames)
                {
                    Directory.Delete($"{item}", true);
                }
            }
        }

        private void PrintAddressAndPort()
        {
            try
            {
                string hostName = Dns.GetHostName();
                string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();

                txtPrintHost.Foreground = System.Windows.Media.Brushes.Green;
                txtPrintHost.Text = $"Host Ready: {myIP}:{Constants.ConfigSocket.Port}";
            }
            catch (Exception ex)
            {
                txtPrintHost.Foreground = System.Windows.Media.Brushes.Red;
                txtPrintHost.Text = $"Host Busy: {ex.Message}";
            }
        }

        #endregion

        #region Binding data

        private readonly short _printerType;

        private void InitBindingData()
        {
            this.DataContext = this;
            PrinterName = "Printer Name: ";
            CardPrintModels = new ObservableCollection<CardPrintModel>();
            lsvHistoryPrint.ItemsSource = CardPrintModels;
        }

        private string printerName;
        public string PrinterName
        {
            get { return printerName; }
            set { printerName = value; OnPropertyChanged("PrinterName"); }
        }

        private ObservableCollection<CardPrintModel> cardPrintModels;
        public ObservableCollection<CardPrintModel> CardPrintModels
        {
            get { return cardPrintModels; }
            set
            {
                cardPrintModels = value;
                OnPropertyChanged("CardPrintModels");
            }
        }

        private void AddItemForListPrintHistory(CardPrintModel item)
        {
            Dispatcher.BeginInvoke((Action)(() =>
            {
                CardPrintModels.Insert(0, item);
            }));
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string newValue)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(newValue));
            }
        }

        #endregion

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                if(server != null)
                {
                    server.Close();
                }

                if(clientList != null)
                {
                    foreach (var item in clientList)
                    {
                        item.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
