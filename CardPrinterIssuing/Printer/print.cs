﻿////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Datacard Corporation.  All Rights Reserved.
//
// XPS Driver SDK csharp print sample.
////////////////////////////////////////////////////////////////////////////////
using System;
using System.Drawing.Printing;
using System.Text;
using dxp01sdk;
using dxp01sdk.mifare;

namespace print
{

    public class Print
    {

        private static string GetHopperStatus(
            BidiSplWrap bidiSpl,
            string hopperId)
        {
            string hopperStatusXml = bidiSpl.GetPrinterData(strings.HOPPER_STATUS);
            string hopperStatus = Util.ParseHopperStatusXML(hopperStatusXml, hopperId);
            return hopperStatus;
        }
        public static string RunMain(string msgId, string[] args)
        {
            string resultPrint = "";
            CommandLineOptions commandLineOptions = CommandLineOptions.CreateFromArguments(args);
            if (commandLineOptions == null)
            {
                return "Driver Data Card not installed";
            }
            commandLineOptions.Validate();
            bool checkHopperStatus = commandLineOptions.hopperID != null &&
                                     commandLineOptions.hopperID.Length > 0 &&
                                     commandLineOptions.checkHopper;

            BidiSplWrap bidiSpl = null;
            int printerJobID = 0;

            try
            {
                bidiSpl = new BidiSplWrap();
                var isBindDevice = bidiSpl.BindDevice(commandLineOptions.printerName);
                if (!isBindDevice)
                {
                    return $"Can not bind {commandLineOptions.printerName}. Please check printer name again.";
                }

                string driverVersionXml = bidiSpl.GetPrinterData(strings.SDK_VERSION);
                Console.WriteLine(Environment.NewLine + "driver version: " + Util.ParseDriverVersionXML(driverVersionXml) + Environment.NewLine);

                string printerOptionsXML = bidiSpl.GetPrinterData(strings.PRINTER_OPTIONS2);
                PrinterOptionsValues printerOptionsValues = Util.ParsePrinterOptionsXML(printerOptionsXML);

                if ("Ready" != printerOptionsValues._printerStatus && "Busy" != printerOptionsValues._printerStatus)
                {
                    throw new Exception(commandLineOptions.printerName + " is not ready. status: " + printerOptionsValues._printerStatus);
                }

                if ("Installed" != printerOptionsValues._printHead)
                {
                    throw new Exception(commandLineOptions.printerName + " does not have a print head installed.");
                }

                if (commandLineOptions.magstripe && !printerOptionsValues._optionMagstripe.Contains("ISO"))
                {
                    throw new Exception(commandLineOptions.printerName + " does not have an ISO magnetic stripe unit installed.");
                }

                if (checkHopperStatus)
                {
                    string hopperStatus = GetHopperStatus(bidiSpl, commandLineOptions.hopperID);
                    if (String.Compare(hopperStatus, "Empty", StringComparison.OrdinalIgnoreCase) == 0)
                        throw new Exception("Hopper '" + commandLineOptions.hopperID + "' is empty.");

                    Console.WriteLine("Status of hopper '" + commandLineOptions.hopperID + "': " + hopperStatus + ".");
                }

                string hopperID = "1";
                if (commandLineOptions.jobCompletion || (commandLineOptions.hopperID != null && commandLineOptions.hopperID.Length > 0))
                {
                    printerJobID = Util.StartJob(bidiSpl, (commandLineOptions.hopperID != null && commandLineOptions.hopperID.Length > 0) ? commandLineOptions.hopperID : hopperID);
                }

                SamplePrintDocument printDocument = new SamplePrintDocument(commandLineOptions, msgId);
                printDocument.PrintController = new StandardPrintController();
                printDocument.BeginPrint += new PrintEventHandler(printDocument.OnBeginPrint);
                printDocument.QueryPageSettings += new QueryPageSettingsEventHandler(printDocument.OnQueryPageSettings);
                printDocument.PrintPage += new PrintPageEventHandler(printDocument.OnPrintPage);
                printDocument.Print();

                if (0 != printerJobID)
                {
                    // wait for the print spooling to finish and then issue an EndJob():
                    Util.WaitForWindowsJobID(bidiSpl, commandLineOptions.printerName);
                    bidiSpl.SetPrinterData(strings.ENDJOB);
                }

                if (commandLineOptions.jobCompletion)
                {
                    Util.PollForJobCompletion(bidiSpl, printerJobID);
                }
            }
            catch (BidiException e)
            {
                Console.WriteLine(e.Message);
                Util.CancelJob(bidiSpl, e.PrinterJobID, e.ErrorCode);
                resultPrint = e.Message;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (0 != printerJobID)
                {
                    Util.CancelJob(bidiSpl, printerJobID, 0);
                }
                resultPrint = e.Message;
            }
            finally
            {
                bidiSpl.UnbindDevice();
            }

            return resultPrint;
        }
    }

    public class SmartCard
    {
        public static void SmartcardPark(BidiSplWrap bidiSpl, bool parkBack)
        {
            var parkCommand = parkBack ? strings.SMARTCARD_PARK_BACK : strings.SMARTCARD_PARK;
            string printerStatusXML = bidiSpl.SetPrinterData(parkCommand);
            PrinterStatusValues printerStatusValues = Util.ParsePrinterStatusXML(printerStatusXML);
            if (0 != printerStatusValues._errorCode)
            {
                throw new Exception("SmartcardPark(): " + printerStatusValues._errorString);
            }
        }

        public static void ResumeJob(BidiSplWrap bidiSpl, int printerJobID, int errorCode)
        {
            string xmlFormat = strings.PRINTER_ACTION_XML;
            string input = string.Format(xmlFormat, (int)Actions.Resume, printerJobID, errorCode);
            Console.WriteLine("issuing Resume after smartcard:");
            bidiSpl.SetPrinterData(strings.PRINTER_ACTION, input);
        }

        private static string GetSCardStatus(SCard scard)
        {
            var states = new int[0];
            var protocol = (uint)scard_protocol.SCARD_PROTOCOL_UNDEFINED;
            var ATRBytes = new byte[0];

            var scardResult = scard.SCardStatus(ref states, ref protocol, ref ATRBytes);

            if (scard_error.SCARD_S_SUCCESS != (scard_error)scardResult)
            {
                return "SCardStatus() fail";
            }

            // display all the 'states' returned. if ANY of the states is SCARD_ABSENT -
            // we are done with this card.
            bool cardAbsent = false;
            for (var index = 0; index < states.Length; index++)
            {
                if (scard_state.SCARD_ABSENT == (scard_state)states[index])
                    cardAbsent = true;
            }

            if (cardAbsent)
            {
                return "One of the states is SCARD_ABSENT.";
            }

            return null;
        }

        private static string LoadKeys(SCard scard, byte sector, byte block, byte[] keys, Mifare_Command.KeyType keyType)
        {
            var command = new Mifare_Command();

            var sendBytes = command.CreateLoadKeysCommand(keyType, sector, keys);

            var receivedBytes = new byte[0];
            var scardResult = scard.SCardTransmit(sendBytes, ref receivedBytes);
            if (scard_error.SCARD_S_SUCCESS == (scard_error)scardResult && receivedBytes.Length > 0)
            {
                return Mifare_Common.CheckStatusCode(receivedBytes[0]);
            }
            else
            {
                return "Prep for write: load keys fail";
            }
        }

        private static string ReadDataInCard(SCard scard, byte sector, byte block, Mifare_Command.KeyType keyType, out string msgError)
        {
            msgError = "";
            var command = new Mifare_Command();
            var sendBytes = command.CreateReadBlockCommand(keyType, sector, block);
            var receivedBytes = new byte[0];
            var scardResult = scard.SCardTransmit(sendBytes, ref receivedBytes);
            if (scard_error.SCARD_S_SUCCESS == (scard_error)scardResult && receivedBytes.Length > 0)
            {
                var apduResponse = new MiFare_Response(receivedBytes);
                if (apduResponse.BlockHasNonzeroData())
                {
                    string statusCode = Mifare_Common.CheckStatusCode(receivedBytes[0]);
                    if (!string.IsNullOrEmpty(statusCode))
                    {
                        msgError = statusCode;
                        return null;
                    }
                }
                else
                {
                    msgError = "Sector is all zeros";
                    return null;
                }
            }
            else
            {
                msgError = string.Format("Read sector {0}, block {1} fail", sector, block);
                return null;
            }

            var readDataString = Encoding.ASCII.GetString(receivedBytes, 1, 16);
            return readDataString;
        }

        private static string WriteDataInCard(SCard scard, byte sector, byte block, Mifare_Command.KeyType keyType, byte[] dataByte)
        {
            var command = new Mifare_Command();
            var sendBytes = command.CreateWriteBlockCommand(keyType, sector, block, dataByte);

            var receivedBytes = new byte[0];
            var scardResult = scard.SCardTransmit(sendBytes, ref receivedBytes);
            if (scard_error.SCARD_S_SUCCESS == (scard_error)scardResult && receivedBytes.Length > 0)
            {
                return Mifare_Common.CheckStatusCode(receivedBytes[0]);
            }
            else
            {
                return "Write data failed.";
            }
        }

        public static string ReadData(BidiSplWrap bidiSpl, byte block, byte sector, byte[] keys, out string msgError, Mifare_Command.KeyType keytype = Mifare_Command.KeyType.B)
        {
            var scard = new SCard(bidiSpl);
            var protocol = (uint)scard_protocol.SCARD_PROTOCOL_DEFAULT;

            var scardResult = scard.SCardConnect(SCard.ChipConnection.contactless, ref protocol);

            if (scard_error.SCARD_S_SUCCESS != (scard_error)scardResult)
            {
                msgError = "SCardConnect() fail";
                return null;
            }

            string errorSCardStatus = GetSCardStatus(scard);
            if (!string.IsNullOrEmpty(errorSCardStatus))
            {
                msgError = errorSCardStatus;
                return null;
            }

            //DisplayChipInfo(scard);
            LoadKeys(scard, sector, block, keys, keytype);
            string data = ReadDataInCard(scard, sector, block, keytype, out msgError);
            return data;
        }

        public static string WriteData(BidiSplWrap bidiSpl, byte block, byte sector, byte[] keys, byte[] data, Mifare_Command.KeyType keytype = Mifare_Command.KeyType.B)
        {
            var scard = new SCard(bidiSpl);
            var protocol = (uint)scard_protocol.SCARD_PROTOCOL_DEFAULT;

            var scardResult = scard.SCardConnect(SCard.ChipConnection.contactless, ref protocol);

            if (scard_error.SCARD_S_SUCCESS != (scard_error)scardResult)
            {
                return "SCardConnect() fail";
            }

            string errorSCardStatus = GetSCardStatus(scard);
            if (!string.IsNullOrEmpty(errorSCardStatus))
            {
                return errorSCardStatus;
            }

            //DisplayChipInfo(scard);
            LoadKeys(scard, sector, block, keys, keytype);
            string msgError = WriteDataInCard(scard, sector, block, keytype, data);
            if(string.IsNullOrEmpty(msgError) || msgError == "OK")
            {
                return null;
            }
            else
            {
                return msgError;
            }
        }
    }
}