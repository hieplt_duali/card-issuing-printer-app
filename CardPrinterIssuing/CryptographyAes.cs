using CardPrinterIssuing.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CardIssue.Infrastructure
{
    public class CryptographyAes
    {
        public static byte[] EncryptStringToBytes(byte[] input, string textHexKey, string textHexIv)
        {
            byte[] encrypted;
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Mode = CipherMode.CBC;
                aesAlg.KeySize = 128;
                aesAlg.BlockSize = 128;
                
                aesAlg.Key = StringToByteArray(textHexKey);

                aesAlg.IV = StringToByteArray(textHexIv);

                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption. 
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        csEncrypt.Write(input, 0, input.Length);

                        //using (var swEncrypt = new StreamWriter(csEncrypt))
                        //{
                        //    //Write all data to the stream.
                        //    swEncrypt.Write(input);
                        //}
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            return encrypted;
        }
        
        public static byte[] EncryptStringToBytes(string hexText, string textHexKey, string textHexIv)
        {
            byte[] encrypted;
            // byte[] iv;

            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Mode = CipherMode.CBC;
                aesAlg.KeySize = 128;
                aesAlg.BlockSize = 128;
                
                aesAlg.Key = StringToByteArray(textHexKey);
                aesAlg.IV = StringToByteArray(textHexIv);
                // iv = aesAlg.IV;

                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption. 
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(StringToByteArray(hexText));
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            return encrypted;
            // byte[] combinedIvCt = new byte[iv.Length + encrypted.Length];
            // Array.Copy(iv, 0, combinedIvCt, 0, iv.Length);
            // Array.Copy(encrypted, 0, combinedIvCt, iv.Length, encrypted.Length);
            //
            // // Return the encrypted bytes from the memory stream. 
            // return combinedIvCt;
        }
        
        public static string DecryptStringFromBytes_Aes(byte[] cipherTextCombined, byte[] key)
        {
            
            // Declare the string used to hold 
            // the decrypted text. 
            string plaintext = null;

            // Create an Aes object 
            // with the specified key and IV. 
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = key;

                byte[] iv = new byte[aesAlg.BlockSize/8];
                byte[] cipherText = new byte[cipherTextCombined.Length - iv.Length];
                
                Array.Copy(cipherTextCombined, iv, iv.Length);
                Array.Copy(cipherTextCombined, iv.Length, cipherText, 0, cipherText.Length);

                aesAlg.IV = iv;

                aesAlg.Mode = CipherMode.CBC;

                // Create a decryptor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }
        
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
        
        public static byte[] StringToByteArray(string hex)
        {
            int numberChars = hex.Length;
            byte[] bytes = new byte[numberChars / 2];
            for (int i = 0; i < numberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }

    public class CardIssueHelper
    {
        // Enc(UID[7] + SectorNo[3] + KeyIndex[1] + Padding[5]) with AES128 CBC
        // Get 6 byte
        public static byte[] GenerateKeys(string aesKey, string aesIv, byte[] uids, byte sectorNo, byte mode = 0x00)
        {
            byte[] keys = new byte[16];

            // 7 byte UID
            Array.Copy(uids, 0, keys, 0, uids.Length);

            // 3 byte sector number [00 00 sectorNumber]
            keys[9] = sectorNo;

            // 1 byte key index: Read Only (Key Index = 0) || Read/Write (Key Index = 1)
            keys[10] = mode;

            // Encrypt
            byte[] keyEnc = CryptographyAes.EncryptStringToBytes(keys, aesKey, aesIv);

            // byte[] result = new byte[6];
            // Array.Copy(keyEnc, 0, result, 0, 6);

            return keyEnc;
        }

        public static List<byte[]> InitDataAllBlock(IssueData issueData)
        {
            var result = new List<byte[]>();
            // block 0x04 (0) - sector 1: 2 byte ID Len + 8 byte UserId + 2 byte Issue Count
            byte[] block0 = new byte[17];
            block0[0] = 0x04;
            string cardId = issueData.CardId;
            byte[] userIdBytes = Encoding.UTF8.GetBytes(cardId);
            int idLen = userIdBytes.Length;

            block0[1] = 0x00;
            block0[2] = (byte)idLen;
            Array.Copy(userIdBytes, 0, block0, 3, idLen);
            block0[11] = (byte)(issueData.IssueCount / 255);
            block0[12] = (byte)(issueData.IssueCount % 255);

            result.Add(block0);

            // block 0x05 (1) - sector 1: 14 byte Perso Date (YYYYMMDDHHmmss)
            byte[] block1 = new byte[17];
            block1[0] = 0x05;
            Array.Copy(Encoding.UTF8.GetBytes(issueData.PersoDate.ToString("yyyyMMddHHmmss")), 0, block1, 1, 14);
            result.Add(block1);

            // block 0x06 (2) - sector 1: 8 byte Start Date Valid, 8 byte End Date Valid
            byte[] block2 = new byte[17];
            block2[0] = 0x06;
            Array.Copy(Encoding.UTF8.GetBytes(issueData.StartValid.ToString("yyyyMMddHHmmss")), 0, block2, 1, 8);
            Array.Copy(Encoding.UTF8.GetBytes(issueData.EndValid.ToString("yyyyMMddHHmmss")), 0, block2, 9, 8);
            result.Add(block2);

            return result;
        }
    }
}