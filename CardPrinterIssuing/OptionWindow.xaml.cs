﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CardPrinterIssuing
{
    /// <summary>
    /// Interaction logic for OptionWindow.xaml
    /// </summary>
    public partial class OptionWindow : Window
    {
        public OptionWindow()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            this.Show();
        }

        private void btnPrinterEntrustSD360_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow((short)Constants.PrinterType.EntrustSD360);
            window.Closed += Window_Closed;
            window.Show();
            this.Hide();
        }

        private void btnPrinterCXD80_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow((short)Constants.PrinterType.CXD80);
            window.Closed += Window_Closed;
            window.Show();
            this.Hide();
        }
    }
}
