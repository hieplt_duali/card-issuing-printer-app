﻿using CardPrinterIssuing.Models;
using dxp01sdk;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;

namespace CardPrinterIssuing
{
    public class PrintHelper
    {
        public static bool BindDevice(string printerName)
        {
            var bidiSpl = new BidiSplWrap();
            try
            {
                var isBindDevice = bidiSpl.BindDevice(printerName);
                if (isBindDevice)
                {
                    bidiSpl.UnbindDevice();
                    bidiSpl = null;
                    return true;
                }
            }
            catch(Exception ex)
            {
                return false;
            }
            
            return false;
        }

        public static string PrintCard(string msgId, string printerName)
        {
            string msgError = print.Print.RunMain(msgId, new string[] { "-n", printerName, "-r", "both", "-2" });
            return msgError;
        }

        public static string WriteAndPrintCard(string msgId, string printerName, IssueData issueData, ConfigKey configKey, out string uid)
        {
            string msgError = singlewire_mifare.smartcard_singlewire_mifare.RunMain(msgId, new string[] { "-n", printerName, "-p", "-c" }, issueData, configKey, out uid);
            return msgError;
        }

        public static bool ReadData(string printerName, byte block, byte sector, byte[] key, out string textData)
        {
            int printerJobID = 0;
            bool status = false;
            BidiSplWrap bidiSpl = new BidiSplWrap();
            try
            {
                bidiSpl.BindDevice(printerName);

                // optional:
                string driverVersionXml = bidiSpl.GetPrinterData(strings.SDK_VERSION);
                Console.WriteLine(Environment.NewLine + "driver version: " + Util.ParseDriverVersionXML(driverVersionXml) + Environment.NewLine);

                string printerOptionsXml = bidiSpl.GetPrinterData(strings.PRINTER_OPTIONS2);
                PrinterOptionsValues printerOptionsValues = Util.ParsePrinterOptionsXML(printerOptionsXml);
                if ("Ready" != printerOptionsValues._printerStatus && "Busy" != printerOptionsValues._printerStatus)
                {
                    throw new Exception(printerName + " is not ready. status: " + printerOptionsValues._printerStatus);
                }

                if ("Single wire" != printerOptionsValues._optionSmartcard)
                {
                    var msg = string.Format("{0} needs 'Single wire' for smartcard option. '{1}' was returned.", printerName, printerOptionsValues._optionSmartcard);
                    throw new Exception(msg);
                }

                string hopperID = string.Empty;
                printerJobID = Util.StartJob(bidiSpl, hopperID);

                print.SmartCard.SmartcardPark(bidiSpl, true);

                textData = print.SmartCard.ReadData(bidiSpl, block, sector, key, out string msgError);
                if(textData == null)
                {
                    throw new Exception(msgError);
                }
                else
                {
                    status = true;
                }

                print.SmartCard.ResumeJob(bidiSpl, printerJobID, 0);
            }
            catch (BidiException e)
            {
                Util.CancelJob(bidiSpl, e.PrinterJobID, e.ErrorCode);
                textData = e.Message;
                status = false;
            }
            catch (Exception e)
            {
                if (0 != printerJobID)
                {
                    Util.CancelJob(bidiSpl, printerJobID, 0);
                }
                textData = e.Message;
                status = false;
            }
            finally
            {
                bidiSpl.UnbindDevice();
            }

            return status;
        }

        public static string WriteData(string printerName, byte block, byte sector, byte[] key, byte[] data)
        {
            int printerJobID = 0;
            string messageError = "";
            BidiSplWrap bidiSpl = new BidiSplWrap();
            try
            {
                bidiSpl.BindDevice(printerName);

                // optional:
                string driverVersionXml = bidiSpl.GetPrinterData(strings.SDK_VERSION);
                Console.WriteLine(Environment.NewLine + "driver version: " + Util.ParseDriverVersionXML(driverVersionXml) + Environment.NewLine);

                string printerOptionsXml = bidiSpl.GetPrinterData(strings.PRINTER_OPTIONS2);
                PrinterOptionsValues printerOptionsValues = Util.ParsePrinterOptionsXML(printerOptionsXml);
                if ("Ready" != printerOptionsValues._printerStatus && "Busy" != printerOptionsValues._printerStatus)
                {
                    throw new Exception(printerName + " is not ready. status: " + printerOptionsValues._printerStatus);
                }

                if ("Single wire" != printerOptionsValues._optionSmartcard)
                {
                    var msg = string.Format("{0} needs 'Single wire' for smartcard option. '{1}' was returned.", printerName, printerOptionsValues._optionSmartcard);
                    throw new Exception(msg);
                }

                string hopperID = string.Empty;
                printerJobID = Util.StartJob(bidiSpl, hopperID);

                print.SmartCard.SmartcardPark(bidiSpl, true);

                messageError = print.SmartCard.WriteData(bidiSpl, block, sector, key, data);
                if (messageError != null)
                {
                    throw new Exception(messageError);
                }

                print.SmartCard.ResumeJob(bidiSpl, printerJobID, 0);
            }
            catch (BidiException e)
            {
                Util.CancelJob(bidiSpl, e.PrinterJobID, e.ErrorCode);
                messageError = e.Message;
            }
            catch (Exception e)
            {
                if (0 != printerJobID)
                {
                    Util.CancelJob(bidiSpl, printerJobID, 0);
                }
                messageError = e.Message;
            }
            finally
            {
                bidiSpl.UnbindDevice();
            }

            return messageError;
        }
    }

    public class PrinterCXD80Helper
    {
        [DllImport("PCP21CT")]
        public static extern int CXCMD_ScanPrinter(out int piSlot, out int piID);

        [DllImport("PCP21CT")]
        public static extern int CXCMD_ScanPrinterNext(out int piSlot, out int piID);

        [DllImport("PCP21CT")]
        public static extern bool CXCMD_CheckIfConnected(out int piSlot, out int piID);

        [DllImport("PCP21CT")]
        public static extern int CXCMD_TestUnitReady(int iSlot, int iID);

        [DllImport("PCP21CT")]
        public static extern int CXCMD_ReadPosition(int iSlot, int iID, out byte[] pbyBuffer);

        [DllImport("PCP21CT")]
        public static extern int CXCMD_ImageOut(int iSlot, int iID, byte[] pbyPlane, int iLength, int iColor, int iBuffer);

        [DllImport("PCP21CT")]
        public static extern int CXCMD_Print(int iSlot, int iID, int iColor, int iBuffer, int iImmed);

        [DllImport("PCP21CT")]
        public static extern int CXCMD_Retransfer(int iSlot, int iID, int iImmed);

        [DllImport("PCP21CT")]
        public static extern int CXCMD_RetransferAndEject(int iSlot, int iID, int iImmed);

        [DllImport("PCP21CT")]
        public static extern int CXCMD_RetransferAndTurn(int iSlot, int iID, int iImmed);

        [DllImport("PCP21CT")]
        public static extern int CXCMD_SecurityPrint(int iSlot, int iID, int iColor, int iBuffer, int iImmed);

        [DllImport("PCP21CT")]
        public static extern int CXCMD_StandardInquiry(int iSlot, int iID, out byte[] pbyBuffer);

        [DllImport("PCP21CT")]
        public static extern int CXCMD_LoadCard(int iSlot, int iID, int iDest, int iFlip, int iFlipInit, int iImmed);

        [DllImport("PCP21CT")]
        public static extern int CXCMD_MoveCard(int iSlot, int iID, int iDest, int iFlip, int iFlipInit, int iImmed);

        #region Funtion proccess print

        public static bool CheckConnection()
        {
            try
            {
                int piSlot, piID;
                int result;

                // scan printer
                result = CXCMD_ScanPrinter(out piSlot, out piID);
                string msgError = CheckResultError(result, "Can not scan printer");
                if (!string.IsNullOrEmpty(msgError))
                    throw new Exception(msgError);

                // check connected
                bool isConnected = CXCMD_CheckIfConnected(out piSlot, out piID);
                if (!isConnected)
                    throw new Exception("Printer not connected");

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }

        public static string PrintCard(string pathFront, string pathBack)
        {
            string msgError = "";

            try
            {
                int piSlot, piID;
                int result;

                // scan printer
                result = CXCMD_ScanPrinter(out piSlot, out piID);
                msgError = CheckResultError(result, "Can not scan printer");
                if (!string.IsNullOrEmpty(msgError))
                    throw new Exception(msgError);

                // check connected
                bool isConnected = CXCMD_CheckIfConnected(out piSlot, out piID);
                if (!isConnected)
                    throw new Exception("Printer not connected");

                // test ready of print
                result = CXCMD_TestUnitReady(piSlot, piID);
                msgError = CheckResultError(result, "Printer not ready");
                if (!string.IsNullOrEmpty(msgError))
                    throw new Exception(msgError);

                // load card
                result = CXCMD_LoadCard(piSlot, piID, 0, 0, 0, 0);
                msgError = CheckResultError(result, "Can not load card to printer");
                if (!string.IsNullOrEmpty(msgError))
                    throw new Exception(msgError);

                // convert image
                var dataColorFront = ConvertRGBtoYMCK(pathFront);
                var dataColorBack = ConvertRGBtoYMCK(pathBack);
                // image out
                CXCMD_ImageOut(piSlot, piID, dataColorFront["Y"].ToArray(), dataColorFront["Y"].Count, 0, 0);
                CXCMD_ImageOut(piSlot, piID, dataColorFront["C"].ToArray(), dataColorFront["C"].Count, 1, 0);
                CXCMD_ImageOut(piSlot, piID, dataColorFront["M"].ToArray(), dataColorFront["M"].Count, 2, 0);
                CXCMD_ImageOut(piSlot, piID, dataColorFront["K"].ToArray(), dataColorFront["K"].Count, 3, 0);
                CXCMD_ImageOut(piSlot, piID, dataColorBack["Y"].ToArray(), dataColorFront["Y"].Count, 0, 1);
                CXCMD_ImageOut(piSlot, piID, dataColorBack["C"].ToArray(), dataColorFront["C"].Count, 1, 1);
                CXCMD_ImageOut(piSlot, piID, dataColorBack["M"].ToArray(), dataColorFront["M"].Count, 2, 1);
                CXCMD_ImageOut(piSlot, piID, dataColorBack["K"].ToArray(), dataColorFront["K"].Count, 3, 1);

                // print front card
                CXCMD_Print(piSlot, piID, 0, 0, 0);
                CXCMD_Print(piSlot, piID, 1, 0, 0);

                // retransfer and turn
                result = CXCMD_RetransferAndTurn(piSlot, piID, 0);
                msgError = CheckResultError(result, "Can not print front side of card");
                if (!string.IsNullOrEmpty(msgError))
                {
                    CXCMD_MoveCard(piSlot, piID, 4, 0, 0, 0);
                    throw new Exception(msgError);
                }

                // print back card
                CXCMD_Print(piSlot, piID, 0, 1, 0);
                CXCMD_Print(piSlot, piID, 1, 1, 0);

                // retransfer and eject
                result = CXCMD_RetransferAndEject(piSlot, piID, 0);
                msgError = CheckResultError(result, "Can not print back side of card");
                if (!string.IsNullOrEmpty(msgError))
                {
                    CXCMD_MoveCard(piSlot, piID, 4, 0, 0, 0);
                    throw new Exception(msgError);
                }

                msgError = "";
            }
            catch (Exception ex)
            {
                msgError = ex.Message;
            }

            return msgError;
        }

        private static string CheckResultError(int resultCode, string msgError = "Something wrong")
        {
            switch (resultCode)
            {
                case 0:
                    return "";
                case 1:
                    return "BUSY: CX Port Manager did not send command to the printer because of Printer's condition";
                case 2:
                    return "TARGET BUSY 1: Card Printer rejected the command because it is on the way of moving the card";
                case 3:
                    return "BUS BUSY: Printer Control DLL rejected the command because the command issued by other proccess is on the way of proccessing";
                case 4:
                    return "TARGET BUSY 2: Card Printer rejected the command because it is on the way of printing on th retransfer film";
                case 5:
                    return "TARGET BUSY 3: Card Printer rejected the command because of both TARGET BUSY 1 and TARGET BUSY 2";
            }

            return msgError;
        }

        private static Dictionary<string, List<byte>> ConvertRGBtoYMCK(string imagePath)
        {
            Dictionary<string, List<byte>> dataColor = new Dictionary<string, List<byte>>();
            List<byte> YColors = new List<byte>();
            List<byte> MColors = new List<byte>();
            List<byte> CColors = new List<byte>();
            List<byte> KColors = new List<byte>();

            //Bitmap img = new Bitmap(imagePath);
            Bitmap img = ResizeImage(imagePath);
            int width = img.Width;
            int height = img.Height;
            int count = 1;

            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    Color pixel = img.GetPixel(i, j);
                    YColors.Add((byte)(255 - pixel.B));
                    MColors.Add((byte)(255 - pixel.G));
                    CColors.Add((byte)(255 - pixel.R));
                    KColors.Add((byte)(255 - pixel.G));

                    count++;
                }
            }

            dataColor.Add("Y", YColors);
            dataColor.Add("M", MColors);
            dataColor.Add("C", CColors);
            dataColor.Add("K", KColors);
            Console.WriteLine($"[Convert Image {imagePath}]: width: {width}, height: {height}");
            Console.WriteLine($"[Data length] Y: {YColors.Count}, M: {MColors.Count}, C: {CColors.Count}, K: {KColors.Count}");
            return dataColor;
        }

        private static Bitmap ResizeImage(string path)
        {
            Bitmap bitmap = new Bitmap(1036, 664);
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphics.DrawImage(System.Drawing.Image.FromFile(path), 0, 0, 1036, 664);
            graphics.Dispose();

            return bitmap;
        }

        #endregion
    }
}
